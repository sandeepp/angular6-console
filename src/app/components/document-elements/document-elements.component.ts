import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-document-elements',
  templateUrl: './document-elements.component.html',
  styleUrls: ['./document-elements.component.scss']
})
export class DocumentElementsComponent implements OnInit {
  fieldsData = [
    {
      title: 'Add Sub Section',
      img: '',
      type: 'section',
      isCollapsed: false,
      isDeleted: false,
      attributes: []
    },
    {
      title: 'Table',
      img: '',
      type: 'table',
      isCollapsed: false,
      isDeleted: false
    },
    {
      title: 'Fields',
      img: '',
      type: 'field',
      isCollapsed: false,
      isDeleted: false
    },
    {
      title: 'OMR Fields',
      img: '',
      type: 'omrFields',
      isCollapsed: false,
      isDeleted: false
    },
    {
      title: 'Free Text',
      img: '',
      type: 'paragraph',
      isCollapsed: false,
      isDeleted: false
    }
  ];
  selectedFields: any = [];
  constructor(private http: HttpClient) {
    this.http.get('/assets/data/sampleData.json').subscribe(data => {
      this.selectedFields = data;
      console.log(this.selectedFields);
    });
  }

  ngOnInit() {
    // this.selectedFields = this.fieldsData;
  }
  selectedElementChange(element) {
    this.selectedFields.push(element);
  }
  elementValueChange(data) {
    if (data.element.isDelete) {
      this.selectedFields.splice(data.index, 1);
    } else {
      this.selectedFields.splice(data.index, 1, data.element.data);
    }
  }
  fieldsDataChange(data) {
    const tempMyObj = Object.assign({}, this.selectedFields[data.index]);
    if (!data['isSubSection']) {
      tempMyObj['data'] = data['fieldsOutputData'];
      this.selectedFields[data.index] = tempMyObj;
    } else {
      tempMyObj['attributes'] = data['fieldsOutputData'];
      this.selectedFields[data.index] = tempMyObj;
    }
    this.selectedFields = this.selectedFields.slice(0);
    console.log(this.selectedFields);
  }
  subSectionElementSelection(data) {
    this.selectedFields = data;
    console.log(this.selectedFields);
  }
}
