import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentElementsComponent } from './document-elements.component';

describe('DocumentElementsComponent', () => {
  let component: DocumentElementsComponent;
  let fixture: ComponentFixture<DocumentElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
