import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  elementData = {
    title: 'Field Name',
    isCollapsed: false,
    img: '',
    isDeleted:  false,
    type: 'field'
  };
}
