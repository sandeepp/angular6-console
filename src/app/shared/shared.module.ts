import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ElementContainerComponent } from './components/element-container/element-container.component';
import { DomainTableComponent } from './components/domain-table/domain-table.component';
import { DomainSubSectionComponent } from './components/domain-sub-section/domain-sub-section.component';
import { DomainFieldsComponent } from './components/domain-fields/domain-fields.component';
import { DomainOmrFieldsComponent } from './components/domain-omr-fields/domain-omr-fields.component';
import { DomainParagraphComponent } from './components/domain-paragraph/domain-paragraph.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { DomainElementsComponent } from './components/domain-elements/domain-elements.component';
import { DisplayFieldsComponent } from './components/display-fields/display-fields.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ElementContainerComponent, DomainTableComponent, DomainSubSectionComponent, DomainFieldsComponent, DomainOmrFieldsComponent, DomainParagraphComponent, DomainElementsComponent, DisplayFieldsComponent],
  exports: [ElementContainerComponent, DomainFieldsComponent,DisplayFieldsComponent,DomainSubSectionComponent, DomainElementsComponent, DomainTableComponent]
})
export class SharedModule { }
