import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainOmrFieldsComponent } from './domain-omr-fields.component';

describe('DomainOmrFieldsComponent', () => {
  let component: DomainOmrFieldsComponent;
  let fixture: ComponentFixture<DomainOmrFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainOmrFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainOmrFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
