import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-domain-sub-section',
  templateUrl: './domain-sub-section.component.html',
  styleUrls: ['./domain-sub-section.component.scss']
})
export class DomainSubSectionComponent implements OnInit {
  @Output() subSectionEmitter = new EventEmitter();
  @Output() subSectionElementSelection = new EventEmitter();
  @Input() fieldsData;
  @Input() index;
  @Input() selectedFields: any[];
  // selectedFields=[];
  constructor() { }
  ngOnInit() {
  }
  selectedElementChange(element) {
    const obj = Object.assign({}, element);
    let arr = [];
    arr = JSON.parse(JSON.stringify(this.selectedFields));
    arr.push(obj);
    this.selectedFields = JSON.parse(JSON.stringify(arr));
    this.subSectionElementSelection.emit(this.selectedFields);
    // this.selectedFields[this.index].attributes = element;
    console.log(this.selectedFields);
  }
  elementValueChange(data) {
    if (data.element.isDelete) {
      this.selectedFields.splice(data.index, 1);
    } else {
      this.selectedFields.splice(data.index, 1, data.element.data);
    }
  }
  dataEmitter(data) {
    this.selectedFields[data.index].data = data['fieldsOutputData'];
    this.subSectionEmitter.emit(this.selectedFields);
  }
  subSectionSelectionData(data) {
    this.selectedFields = data;
    this.subSectionEmitter.emit(this.selectedFields);
  }

}
