import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainSubSectionComponent } from './domain-sub-section.component';

describe('DomainSubSectionComponent', () => {
  let component: DomainSubSectionComponent;
  let fixture: ComponentFixture<DomainSubSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainSubSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainSubSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
