import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-display-fields',
  templateUrl: './display-fields.component.html',
  styleUrls: ['./display-fields.component.scss']
})
export class DisplayFieldsComponent implements OnInit, OnChanges {
  @Output() deleteElementChange = new EventEmitter();
  @Output() dataEmitter = new EventEmitter();
  @Output() subSectionElementChange = new EventEmitter();
  @Input() fieldsData;
  @Input() selectedFields;
  constructor() { }
  ngOnChanges() {
  }
  ngOnInit() {
  }
  elementValueChange(element, i) {
    this.deleteElementChange.emit({ element: element, index: i });
  }
  addOutputData(fieldsOutputData, fieldInitialData, index, isSubSection?) {
    this.dataEmitter.emit({fieldsOutputData, fieldInitialData, index, isSubSection});
  }
  subSectionElementSelection(data, index) {
    this.selectedFields[index].attributes = data;
    this.subSectionElementChange.emit(this.selectedFields);
  }
}
