import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-domain-fields',
  templateUrl: './domain-fields.component.html',
  styleUrls: ['./domain-fields.component.scss']
})
export class DomainFieldsComponent implements OnInit {
  @Output() fieldsDataChange = new EventEmitter();
  myForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      isHeadingAvailable: [''],
      isFieldSizeVariable: [''],
      selectionType: [''],
      variableName: [''],
      dataType: [''],
      areRulesEnabled: ['']
    });
    this.myForm.valueChanges.subscribe(data => {
      console.log(this.myForm.value);
    });
  }
  onSaveClick() {
    this.fieldsDataChange.emit(this.myForm.value);
  }


}
