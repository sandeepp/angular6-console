import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainFieldsComponent } from './domain-fields.component';

describe('DomainFieldsComponent', () => {
  let component: DomainFieldsComponent;
  let fixture: ComponentFixture<DomainFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
