import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainElementsComponent } from './domain-elements.component';

describe('DomainElementsComponent', () => {
  let component: DomainElementsComponent;
  let fixture: ComponentFixture<DomainElementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainElementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainElementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
