import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-domain-elements',
  templateUrl: './domain-elements.component.html',
  styleUrls: ['./domain-elements.component.scss']
})
export class DomainElementsComponent implements OnInit {
  @Output() selectedElementChange = new EventEmitter();
  @Input() fieldsData;
  constructor() { }

  ngOnInit() {
  }
  onElementClick(element) {
    this.selectedElementChange.emit(element);
  }

}
