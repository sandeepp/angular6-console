import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-domain-table',
  templateUrl: './domain-table.component.html',
  styleUrls: ['./domain-table.component.scss']
})
export class DomainTableComponent implements OnInit {
  @Output() tableDataEmitter = new EventEmitter();
  tableForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.tableForm = this.fb.group({
      startIdentifier: [''],
      endIdentifier: [''],
      isTranspose: ['']
    });
    this.tableForm.valueChanges.subscribe(data => {
      console.log(data);
    });
  }
  onSaveClick(tableForm) {
    console.log(tableForm);
    this.tableDataEmitter.emit(tableForm.value);

  }

}
