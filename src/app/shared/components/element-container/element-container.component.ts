import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-element-container',
  templateUrl: './element-container.component.html',
  styleUrls: ['./element-container.component.scss']
})
export class ElementContainerComponent implements OnInit {
  @Input() elementData;
  @Output() elementValueChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  onDeleteClick(data) {
    data.isDeleted = true;
    this.elementValueChange.emit({ data: data, isDelete: true });
  }
  onExpandClick(data) {
    const tempMyObj = Object.assign({}, data);
    if (tempMyObj['isCollapsed']) {
      tempMyObj['isCollapsed'] = false;
    } else {
      tempMyObj['isCollapsed'] = true;
    }
    this.elementValueChange.emit({ data: tempMyObj, isDelete: false});

  }
}
