import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainParagraphComponent } from './domain-paragraph.component';

describe('DomainParagraphComponent', () => {
  let component: DomainParagraphComponent;
  let fixture: ComponentFixture<DomainParagraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DomainParagraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainParagraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
